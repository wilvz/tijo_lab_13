package com.demo.springboot.domain.dto;

import java.io.Serializable;

public class MovieDto implements Serializable {

    private int movieId;
    private String title;
    private int year;
    private String image;

    public MovieDto() {
    }

    public MovieDto(int movieId, String title, int year, String image) {
        this.movieId = movieId;
        this.title = title;
        this.year = year;
        this.image = image;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public int hashCode() {
        return this.getMovieId();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) {
            return false;
        }
        if (obj == this)
            return true;
        if (!(obj instanceof MovieDto))
            return false;

        MovieDto other = (MovieDto)obj;
        return (this.getMovieId()==other.getMovieId());
    }
}
