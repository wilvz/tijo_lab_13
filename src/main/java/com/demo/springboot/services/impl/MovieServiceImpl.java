package com.demo.springboot.services.impl;

import com.demo.springboot.domain.dto.MovieDto;
import com.demo.springboot.exception.ItemAlreadyInList;
import com.demo.springboot.services.MovieService;
import com.opencsv.CSVWriter;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;



@Service
public class MovieServiceImpl implements MovieService {
    private String fileName = "src/main/resources/movies.csv";

    @Override
    public List<MovieDto> readMovies() {

        List<MovieDto> movies = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            br.readLine();
            while ((line = br.readLine()) != null) {

                String[] values = line.split(";");

                movies.add(new MovieDto(Integer.parseInt(values[0].trim()),values[1].trim(),
                                        Integer.parseInt(values[2].trim()), values[3].trim()));

            }
        }catch (IOException e) {
            e.printStackTrace();
        }

        return movies;
    }

    @Override
    public void addNewMovie(MovieDto movie) throws ItemAlreadyInList {
        List<MovieDto> movies = readMovies();

        if(!movies.contains(movie)){
            try (CSVWriter writer = new CSVWriter(
                    new FileWriter(fileName, true),
                    ';',
                    CSVWriter.NO_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                    ";\r\n")){

                String[] data = {
                        Integer.toString(movie.getMovieId()),
                        movie.getTitle(),
                        Integer.toString(movie.getYear()),
                        movie.getImage()
                };
                writer.writeNext(data);
            } catch (IOException error) {
                System.out.println("addMovie IOException!");
            }

        }else{
            throw new ItemAlreadyInList("movie exist in a list");
        }



    }
}
