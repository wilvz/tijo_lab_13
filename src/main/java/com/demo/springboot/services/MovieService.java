package com.demo.springboot.services;

import com.demo.springboot.domain.dto.MovieDto;
import com.demo.springboot.exception.ItemAlreadyInList;

import java.util.List;

public interface MovieService {
    List<MovieDto> readMovies();

    void addNewMovie(MovieDto movie) throws ItemAlreadyInList;
}
