package com.demo.springboot.exception;

public class ItemAlreadyInList extends Exception {
    public ItemAlreadyInList(String errorMessage) {
        super(errorMessage);
    }
}
