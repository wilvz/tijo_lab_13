package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.MovieDto;
import com.demo.springboot.domain.dto.MovieListDto;
import com.demo.springboot.exception.ItemAlreadyInList;
import com.demo.springboot.services.MovieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MovieApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);
    private final MovieService movieService;

    @Autowired
    public MovieApiController(MovieService movieService) {
        this.movieService = movieService;
    }

    @RequestMapping(value = "/movies")
    @ResponseBody
    @CrossOrigin
    public MovieListDto getMovies() {

        LOGGER.info("--- get movies");
        List<MovieDto> movies = movieService.readMovies();
        return new MovieListDto(movies);
    }

    @RequestMapping(value = "/movies", method = RequestMethod.POST)
    @CrossOrigin
    public ResponseEntity<?> createMovie(@RequestBody MovieDto movieDto) {

        LOGGER.info("--- add movie");

        try{
            movieService.addNewMovie(movieDto);

        }catch (ItemAlreadyInList e){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);


    }
}
